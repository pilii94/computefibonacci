import static org.junit.Assert.*;

import org.junit.Test;

public class FibonacciTest {

	@Test
	
	public void shouldFibonacciOfZeroReturnZero() {
		Fibonacci fibonacci=new Fibonacci();
		int value=fibonacci.compute(0);
		int expectedvalue=0;
		assertEquals(expectedvalue, value);
		
	}
	@Test
	public void shouldFibonacciOfOneReturnOne(){
		Fibonacci fibonacci = new Fibonacci();
		int value=fibonacci.compute(1);
		int expectedvalue=1;
		assertEquals(expectedvalue, value);
	}
	@Test
	public void shouldFibonacciOfTwoReturnOne(){
		Fibonacci fibonacci=new Fibonacci();
		int value=fibonacci.compute(2);
		int expectedvalue=1;
		assertEquals(expectedvalue, value);
	}
	@Test(expected=RuntimeException.class)
	public void shouldFactorialOfNegativeNumberRaiseException(){
		Fibonacci fibonacci=new Fibonacci();
		fibonacci.compute(-1);
	}

}
