
public class Fibonacci {
	
		public int compute(int value) {
				int result=0;
				
				if(value<0){
					throw new RuntimeException("Error: received negative value");
				}if   (value <= 1){ 
					result= value;
			}else {
					result= compute(value-1)+compute(value-2);
				}
				return result;
				}
			
	}

